#!/bin/sh -v

sudo xbps-remove --clean-cache
sudo xbps-remove --clean-cache --cachedir /var/cache/xbps-src/binpkgs
sudo xbps-remove --clean-cache --cachedir /var/cache/xbps-src/repocache
sudo xbps-remove --clean-cache --cachedir /var/cache/xbps-src/repocache-x86_64-musl
( cd /var/tmp/void-packages && ./xbps-src -H /var/cache/xbps-src purge-distfiles )
