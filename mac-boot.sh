#!/bin/sh

cd /boot || exit 1

rm -rf  System  ".HFS+ Private Directory Data"*  .Spotlight-*  .Trashes  .fseventsd  # optional
touch mach_kernel
mkdir -p System/Library/CoreServices

cat << EOF > System/Library/CoreServices/SystemVersion.plist
<?xml version="1.0" encoding="utf-8"?>
<plist version="1.0">
	<dict>
		<key>ProductBuildVersion</key>
		<string></string>
		<key>ProductName</key>
		<string>$(uname -o)</string>
		<key>ProductVersion</key>
		<string>$(xbps-query -s linux | grep -Po 'linux\K[0-9]+\.[0-9]+' | sort --version-sort | tail -n 1)</string>
	</dict>
</plist>
EOF

echo 'configfile ${cmdpath}/grub.cfg' > /tmp/grub.cfg
grub-mkstandalone \
	-o /boot/System/Library/CoreServices/boot.efi \
	-d /usr/lib/grub/x86_64-efi \
	-O x86_64-efi \
	--fonts="" \
	--modules="part_gpt hfsplus" \
	boot/grub/grub.cfg=/tmp/grub.cfg &&
grub-mkconfig -o /boot/System/Library/CoreServices/grub.cfg

rm /tmp/grub.cfg
