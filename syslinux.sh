#!/bin/sh -v

### TODO: conditionals based on LVM or not, this doesn't work without... something like:
### APPEND root=UUID=$(lsblk -o NAME,FSTYPE,LABEL,UUID | grep -E '\*nix' | awk '{print $4}') loglevel=4 add_efi_memmap rw rd.auto=1

### lsblk needs root
if [ "$(whoami)" != "root" ]; then
	echo "root required."
	exit 1
fi

### TODO: this assumes / is the only encrypted partition
if lsblk --fs --noheadings --output FSTYPE,UUID | grep -q crypto_LUKS; then
	UUID="cryptdevice=UUID=\"$(lsblk --fs --noheadings --output FSTYPE,UUID | grep crypto_LUKS | awk '{print $2}' | head -n 1)\":lvm root=/dev/mapper/base-void resume=/dev/mapper/base-swap"
else
	UUID="root=UUID=$(lsblk --fs --noheadings --output LABEL,UUID | grep '\*nix' | awk '{print $2}' | head -n 1)"
fi

SYSLINUXCFG=/boot/EFI/syslinux/syslinux.cfg
mv ${SYSLINUXCFG} ${SYSLINUXCFG}~
cat << EOF >> ${SYSLINUXCFG]
PROMPT 1
TIMEOUT 5
DEFAULT void
APPEND ${UUID} loglevel=4 add_efi_memmap rw rd.auto=1
EOF

generate_label() {
	! $(grep --quiet "LABEL void${1}$" ${SYSLINUXCFG}) &&
		cat << EOF >> ${SYSLINUXCFG}

LABEL void${1}
	LINUX /vmlinuz-${V1}.${V2}.${V3}_${V4}
	INITRD /initramfs-${V1}.${V2}.${V3}_${V4}.img
EOF
}

for FILE in $(ls /boot/vmlinuz-[0-9]*.[0-9]*.[0-9]*_[0-9]* | sort --reverse --version-sort)
do
	V1=$(echo ${FILE} | sed -E 's|/boot/vmlinuz-([0-9]+?)\.[0-9]+?\.[0-9]+?_[0-9]+?|\1|g')
	V2=$(echo ${FILE} | sed -E 's|/boot/vmlinuz-[0-9]+?\.([0-9]+?)\.[0-9]+?_[0-9]+?|\1|g')
	V3=$(echo ${FILE} | sed -E 's|/boot/vmlinuz-[0-9]+?\.[0-9]+?\.([0-9]+?)_[0-9]+?|\1|g')
	V4=$(echo ${FILE} | sed -E 's|/boot/vmlinuz-[0-9]+?\.[0-9]+?\.[0-9]+?_([0-9]+?)|\1|g')

	generate_label
	generate_label ${V1}.${V2}
	generate_label ${V1}.${V2}.${V3}
	generate_label ${V1}.${V2}.${V3}_${V4}
done
