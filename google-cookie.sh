#!/bin/sh

GOOGLE_COOKIE="${@}"
ID="$(echo "${GOOGLE_COOKIE}" | grep -Po 'ID=\K[^:'\'']+')"
TM="$(echo "${GOOGLE_COOKIE}" | grep -Po 'TM=\K[^:'\'']+')"
S="$(echo "${GOOGLE_COOKIE}" | grep -Po 'S=\K[^:'\'']+')"
#DV="$(echo "${GOOGLE_COOKIE}" | grep -Eo 'DV=\K[^:'\'']+')"

sed -E -i -e s/'ID=[^:'\'']+'/ID=${ID-000}/g "${HOME}/bin/google.sh"
sed -E -i -e s/'TM=[^:'\'']+'/TM=${TM-000}/g "${HOME}/bin/google.sh"
sed -E -i -e s/'S=[^:'\'']+'/S=${S-000}/g "${HOME}/bin/google.sh"
#sed -E -i -e s/'DV=[^:'\'']+'/DV=${DV-000}/g "${HOME}/bin/google.sh"
