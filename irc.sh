#!/bin/sh -v

: ${IRCUSER:=$(date | shasum -a 256 | base64 | tr -cd '[:lower:]' | cut -c -7)}

sic -h ${IRCSERVER:=chat.freenode.net} -p ${IRCPORT:=6667} -n $IRCUSER -k $IRCPASS 2>&1 |
	sed -E \
	-e 's/^(xbps|void)(-[A-Za-z]+)(.*)(NOTICE|JOIN|PART)(.*)/'"$(tput bold)$(tput setaf 0)"'\1\2\3\4\5'"$(tput sgr0)$(tput setaf 7)"'/g' \
	-e '/ NICK .*\(\):/d' \
	-e '/ MODE .*\(.*\):/d' \
	-e '/ JOIN .*\(#.*\):/d' \
	-e '/ PART .*\(#.*\):/d' \
	-e '/ QUIT .*\(\):/d' \
	-e 's/('"${IRCUSER}"')/'"$(tput bold)$(tput setaf 6)"'\1'"$(tput sgr0)$(tput setaf 7)"'/g' \

