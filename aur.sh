#!/bin/sh -v

PACKAGE="$1"

cd /tmp
rm -rf ${PACKAGE} ${PACKAGE}.tar.gz
curl -O https://aur.archlinux.org/cgit/aur.git/snapshot/${PACKAGE}.tar.gz
tar xvf ${PACKAGE}.tar.gz
cd ${PACKAGE}
makepkg -si --needed
cp ${PACKAGE}-*.pkg.tar.xz /var/aur/
