#!/bin/sh -v

xbps-install --repository=/var/cache/xbps-src/binpkgs -A texlive2019-exec-luatex

curl -o /tmp/install-getnonfreefonts https://tug.org/fonts/getnonfreefonts/install-getnonfreefonts
texlua /tmp/install-getnonfreefonts
getnonfreefonts --sys --all
rm /tmp/install-getnonfreefonts
