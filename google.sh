#!/bin/sh

FIND="${1}"
IGNORE="${2}"
LANG="${3}"
REGION="${4}"
DOMAIN="${5}"
curl -L -s -G \
--data-urlencode 'hl=en'                      $(: UI language) \
--data-urlencode 'as_q='                      $(: all these words) \
--data-urlencode 'as_epq='"${FIND}"           $(: this exact word or phrase) \
--data-urlencode 'as_oq='                     $(: any of these words) \
--data-urlencode 'as_eq='"${IGNORE}"          $(: none of these words) \
--data-urlencode 'as_nlo='                    $(: number range low) \
--data-urlencode 'as_nhi='                    $(: number range high) \
--data-urlencode 'lr=lang_'"${LANG}"          $(: search results language) \
--data-urlencode 'cr='"${REGION}"             $(: search results region) \
--data-urlencode 'as_qdr='                    $(: period) \
--data-urlencode 'as_sitesearch='"${DOMAIN}"  $(: site or domain) \
--data-urlencode 'as_occt=body'               $(: where in the page to search) \
--data-urlencode 'safe='                      $(: SafeSearch) \
--data-urlencode 'as_filetype='               $(: file type) \
--data-urlencode 'tbs='                       $(: license) \
'https://www.google.com/search' \
-H 'User-Agent: Mozilla/5.0 (Windows NT 10.0; rv:78.0) Gecko/20100101 Firefox/78.0' \
-H 'Accept: text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,*/*;q=0.8' \
-H 'Accept-Language: en-US,en;q=0.5' \
--compressed \
-H 'DNT: 1' \
-H 'Connection: keep-alive' \
-H 'Cookie: GOOGLE_ABUSE_EXEMPTION=ID=0000000000000000:TM=0000000000:C=r:IP='"$(dig +short myip.opendns.com @resolver1.opendns.com)"'-:S=0000000000000000000000000000000000' \
-H 'Upgrade-Insecure-Requests: 1' \
-H 'Pragma: no-cache' \
-H 'Cache-Control: no-cache' |
	grep -P -o '<div id="result-stats">(About )?\K[^r]+(?= results)'
