#!/bin/sh -v

#CSS_FILE="/tmp/font-face.css"
#rm "${CSS_FILE}"

GENERATE_ONE_STYLE() {

FONT_FILE="${1}"

MIME_TYPE="$(file --brief --mime-type "${FONT_FILE}")"
MIME_ENCODING="$(file --brief --mime-encoding "${FONT_FILE}")"
FONT_FAMILY="$(fc-query -f '%{family}' "${FONT_FILE}" | sed -E 's/,.*//g')"
FONT_STYLE="$(fc-query -f '%{style}' "${FONT_FILE}" | tr 'A-Z' 'a-z' | grep -Eo 'italic' | sort -u)"
FONT_WEIGHT="$(fc-query -f '%{style}' "${FONT_FILE}" | tr 'A-Z' 'a-z' | grep -Eo '(semi|extra|ultra)?bold' | sort -u | tail -n 1 | sed -E -e 's/semibold/600/g' -e 's/bold/700/g')"
BASE64="$(base64 "${FONT_FILE}" | tr -d '\n')"

if echo "${MIME_TYPE}" | grep -Eq 'sfnt'; then
	FONT_FORMAT="truetype"
elif echo "${MIME_TYPE}" | grep -Eq 'opentype'; then
	FONT_FORMAT="opentype"
fi

CSS_FILE="/tmp/$(echo "${FONT_FAMILY}" | tr 'A-Z ' 'a-z-')-$(echo "${FONT_STYLE:-normal}" | head -c 1)$(echo "${FONT_WEIGHT:-400}" | head -c 1).css"
rm "${CSS_FILE}"

cat << EOF >> "${CSS_FILE}"
@font-face { font-style: ${FONT_STYLE:-normal}; font-weight: ${FONT_WEIGHT:-400}; font-family: "${FONT_FAMILY}";
             src: url(data:${MIME_TYPE};charset=${MIME_ENCODING};base64,${BASE64}) format("${FONT_FORMAT}"); }
EOF

}

GENERATE_ONE_STYLE "${1}"
GENERATE_ONE_STYLE "${2}"
GENERATE_ONE_STYLE "${3}"
GENERATE_ONE_STYLE "${4}"
