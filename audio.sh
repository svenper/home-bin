#!/bin/sh -v

[ "${PWD}" = "${HOME}" ] && exit
[ -x "$(command -v cueprint)" ] || exit 
[ -x "$(command -v shnsplit)" ] || exit 
[ -x "$(command -v ffmpeg)" ] || exit 

rename '.CUE' '.cue' *.CUE

shnsplit -f *.cue -t "%n %t" *.flac

sed -Ei '/DATE/s/"//g' *.cue

sed -Ei '/(PERFORMER|TITLE)/s/ A / a /g' *.cue
sed -Ei '/(PERFORMER|TITLE)/s/ An / an /g' *.cue
sed -Ei '/(PERFORMER|TITLE)/s/ And / and /g' *.cue
sed -Ei '/(PERFORMER|TITLE)/s/ As / as /g' *.cue
sed -Ei '/(PERFORMER|TITLE)/s/ At / at /g' *.cue
sed -Ei '/(PERFORMER|TITLE)/s/ But / but /g' *.cue
sed -Ei '/(PERFORMER|TITLE)/s/ By / by /g' *.cue
sed -Ei '/(PERFORMER|TITLE)/s/ For / for /g' *.cue
sed -Ei '/(PERFORMER|TITLE)/s/ In / in /g' *.cue
sed -Ei '/(PERFORMER|TITLE)/s/ Nor / nor /g' *.cue
sed -Ei '/(PERFORMER|TITLE)/s/ Of / of /g' *.cue
sed -Ei '/(PERFORMER|TITLE)/s/ On / on /g' *.cue
sed -Ei '/(PERFORMER|TITLE)/s/ Or / or /g' *.cue
sed -Ei '/(PERFORMER|TITLE)/s/ The / the /g' *.cue
sed -Ei '/(PERFORMER|TITLE)/s/ To / to /g' *.cue
sed -Ei '/(PERFORMER|TITLE)/s/ With / with /g' *.cue

sed -Ei '/(PERFORMER|TITLE)/s/ Y / y /g' *.cue
sed -Ei '/(PERFORMER|TITLE)/s/ La / la /g' *.cue
sed -Ei '/(PERFORMER|TITLE)/s/ Las / las /g' *.cue
sed -Ei '/(PERFORMER|TITLE)/s/ El / el /g' *.cue
sed -Ei '/(PERFORMER|TITLE)/s/ Los / los /g' *.cue

sed -Ei '/(PERFORMER|TITLE)/s/ Et / et /g' *.cue

# workaround for extraneous quotes in metadata
sed -Ei '/(PERFORMER|TITLE)/s/"([^ ]*)"/\1/g' *.cue

# cueprint program doesn't understand that *.cue is a single file if it contains spaces
# OR, if fields are empty, things go crazy
ARTIST="$(find . -name "*.cue" -exec cueprint -d %P {} \; | sed -E 's/"([^ ]*)"/\1/g' | tr -d '\r\n')"
ALBUM="$(find . -name "*.cue" -exec cueprint -d %T {} \; | sed -E 's/"([^ ]*)"/\1/g' | tr -d '\r\n')"
DATE="$(find . -name "*.cue" -exec cat {} \; | grep -E 'REM DATE [0-9]{4}' | grep -E -o '[0-9]{4}')"

rm -rf *pregap*.wav
TRACK=1
for FILE in *.wav
do
    TITLE="$(cueprint -n "${TRACK}" *.cue | grep 'title:\s*' | sed 's/title:\s*//g' | tr -d '\r\n')"
    ffmpeg -loglevel warning -y      \
        -i "${FILE}"                 \
	-movflags +faststart         \
        -metadata title="${TITLE}"   \
        -metadata artist="${ARTIST}" \
        -metadata date="${DATE}"     \
        -metadata track="${TRACK}"   \
        -metadata album="${ALBUM}"   \
        -acodec alac                 \
        "$(printf %02d ${TRACK}) $(echo ${TITLE} | tr '.~/!?' '_').m4a"
    TRACK="$((${TRACK}+1))"
done

[ -e audio.jpg ] && IMAGE_EXT="jpg"
[ -e audio.jpeg ] && IMAGE_EXT="jpeg"
[ -e audio.png ] && IMAGE_EXT="png"
find . -name "*.m4a" -exec AtomicParsley {} --albumArtist "${ARTIST}" --artwork "audio.${IMAGE_EXT}" --overWrite \;

mkdir -p "${ARTIST}/${ALBUM}"
mv *.m4a "${ARTIST}/${ALBUM}"
#rm *.wav
