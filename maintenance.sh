#!/bin/sh

if [ "$(whoami)" != "root" ]; then
	echo "root required."
	exit 1
fi

unset MAINTENANCE_ONLINE
ONLINE_STATUS="$(xbps-query -R --memory-sync --list-repos --verbose)"
if [ "${ONLINE_STATUS}" = "" ] || [ $(echo ${ONLINE_STATUS} | grep -q '\-1') ]; then
	echo "One or more repos seem unavailable."
	MAINTENANCE_ONLINE=""
fi

if ! findmnt -n /boot | grep -Eq '\brw\b'; then
	echo "/boot is not writable"
	exit 1
fi

# at this stage of the script, only deletes old ones, not the booted one
vkpurge rm all

#xbps-remove --clean-cache
#xbps-remove --clean-cache --cachedir /var/cache/xbps-src/repocache
#xbps-remove --clean-cache --cachedir /var/cache/xbps-src/repocache-x86_64-musl

# twice in case xbps itself needs update
${MAINTENANCE_ONLINE-xbps-install --sync}
xbps-install libxbps xbps
xbps-install xbps-triggers
xbps-install --update
#xbps-remove --remove-orphans

(
cd /var/cache/xbps-src/repocache-x86_64-musl &&
for SYNC_CACHE in $(cd /var/cache/xbps/ && ls -1); do
	[ -f ${SYNC_CACHE} ] && rm ${SYNC_CACHE}
	cp /var/cache/xbps/${SYNC_CACHE} ${SYNC_CACHE}
	chown ${SUDO_USER-${USER}}:${SUDO_USER-${USER}} ${SYNC_CACHE}
done
)

# ${MAINTENANCE_ONLINE-tlmgr update --self}
# ${MAINTENANCE_ONLINE-tlmgr update --all}
# 
# mktexlsr --quiet
# fmtutil --quiet --sys --all 1>/dev/null
# updmap --quiet --sys --syncwithtrees &
# #updmap --quiet --sys

# (cd /var/tmp/void-packages && sudo -u ${SUDO_USER-${USER}} git pull) &
# (cd /var/tmp/aports && sudo -u ${SUDO_USER-${USER}} git pull) &
# (cd /var/tmp/gentoo && sudo -u ${SUDO_USER-${USER}} git pull) &
makewhatis /usr/share/man &

if [ -d /boot/EFI/syslinux ] || [ -d /boot/syslinux ]; then
	"$(dirname $(command -v ${0}))/syslinux.sh" &
elif cat /sys/devices/virtual/dmi/id/*vendor | grep -Eqi '\bapple\b'; then
	"$(dirname $(command -v ${0}))/mac-boot.sh" &
else
	echo "You messed up the bootloader!"
fi

wait

updatedb

sync
