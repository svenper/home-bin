#!/bin/sh

PACKAGE="${1}"
TEXDOCS="/tmp/texdocs"
mkdir -p ${TEXDOCS}
cd ${TEXDOCS}
[ -e ${PACKAGE}.pdf ] ||
	curl -Ls -o ${PACKAGE}.pdf $(curl -Ls https://ctan.org/pkg/${PACKAGE} |
		sed -E 's|\&shy;||g' |
		sed -E 's|><|>\n<|g' |
		grep -Ei '(document|manual|package|reference)' |
		grep -Eiv '(source code|example|list of)' |
		grep -Eo '"https?://mirrors.ctan.org/[^"]+\.pdf"' |
		sed -e 's|^"||g' -e 's|"$||g'
		)
(mupdf ${PACKAGE}.pdf &)
