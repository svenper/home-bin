#!/bin/sh -v

echo 32 | sudo tee /sys/class/backlight/intel_backlight/brightness
echo 16 | sudo tee /sys/class/leds/smc::kbd_backlight/brightness

sudo setfont latarcyrheb-sun32
sudo setfont ter-132b

cat << EOF | sudo tee /etc/resolv.conf
nameserver 8.8.8.8
nameserver 8.8.4.4
EOF

cat << EOF | tee $HOME/.zshrc
autoload -U compinit
compinit
EOF

PS1='$ ' exec zsh
