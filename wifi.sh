#!/bin/sh -v

if [ "$(whoami)" != "root" ]; then
	echo "root required"
	exit 1
fi

WL_DEVICE=${WL_DEVICE-unset}
WL_SSID=${WL_SSID-unset}
WL_IP=${WL_IP-unset}
WL_ROUTE=${WL_ROUTE-unset}
WL_IDENTITY=${WL_IDENTITY-unset}
WL_PASS=${WL_PASS-unset}
WL_DISABLE=${WL_DISABLE-unset}

disable() {
	sv down wpa_supplicant
	pkill -x wpa_supplicant
	ip addr flush dev $WL_DEVICE
	ip route flush dev $WL_DEVICE
	ip link set dev $WL_DEVICE down
}

if [ "$WL_DEVICE" = unset ]
then
	PRINT_DEVICES() {
		iw dev | grep Interface | awk '{print $2}'
	}
	if [ $(PRINT_DEVICES | wc -l) = 1 ]
	then
		WL_DEVICE=$(PRINT_DEVICES)
	elif [ "$WL_DEVICE" = unset ]
	then
		echo "These are the available WLAN devices:"
		PRINT_DEVICES
		printf "WLAN device: "
		read WL_DEVICE
	fi
fi

if [ "$WL_DISABLE" != unset ]
then
	disable
	sv restart wpa_supplicant &&
	exit 0
fi

if [ "$WL_SSID" = unset ]
then
	printf "SSID: "
	read WL_SSID
fi

if [ "$WL_IP" = unset ]
then
	printf "IP (blank for auto): "
	read WL_IP
fi

if [ "$WL_ROUTE" = unset ]
then
	printf "Route (blank for auto): "
	read WL_ROUTE
fi

if [ "$WL_IDENTITY" = unset ]
then
	printf "Network identity (blank for none): "
	read WL_IDENTITY
fi

if [ "$WL_PASS" = unset ]
then
	stty -echo
	printf "Network password (blank for none): "
	read WL_PASS
	stty echo
	echo
fi

disable
ip link set dev $WL_DEVICE up

if [ "$WL_IDENTITY" != "" ] && [ "$WL_PASS" != "" ]
then
	rm -f /tmp/wpaconf
	touch /tmp/wpaconf
	chown root:root /tmp/wpaconf
	chmod 600 /tmp/wpaconf
	cat << EOF | tee /tmp/wpaconf 1>/dev/null
$(cat /etc/wpa_supplicant/wpa_supplicant.conf)
network={
	ssid="$WL_SSID"
	key_mgmt=WPA-EAP
	eap=PEAP
	scan_ssid=1
	identity="$WL_IDENTITY"
	password=hash:$(echo -n "$WL_PASS"	 |
	iconv -t utf16le  |
	openssl md4       |
	awk '{print $2}'  )
}
EOF
wpa_supplicant -B -i $WL_DEVICE -c /tmp/wpaconf
rm -f /tmp/wpaconf
elif [ "$WL_IDENTITY" = "" ] && [ "$WL_PASS" != "" ]
then
	rm -f /tmp/wpaconf
	touch /tmp/wpaconf
	chown root:root /tmp/wpaconf
	chmod 600 /tmp/wpaconf
	cat << EOF | tee /tmp/wpaconf 1>/dev/null
$(wpa_passphrase "$WL_SSID" "$WL_PASS")
EOF
	wpa_supplicant -B -i $WL_DEVICE -c /tmp/wpaconf
	rm -f /tmp/wpaconf
elif [ "$WL_IDENTITY" = "" ] && [ "$WL_PASS" = "" ]
then
	iw dev $WL_DEVICE connect "$WL_SSID"
else
	echo "This shouldn't happen. Make sure to provide any passwords and/or usernames."
fi

if $(echo $WL_IP | grep -Eq '([0-9]{1,3}\.){3}[0-9]{1,3}')
then
	sv down dhcpcd
	ip addr flush dev $WL_DEVICE
	ip addr add $WL_IP/24 dev $WL_DEVICE
	ip route add default via ${WL_ROUTE:-192.168.0.1}
else
	sv up dhcpcd
fi
