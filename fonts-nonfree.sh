#!/bin/sh -v


cd /etc/fonts/
mkdir -p apple
mkdir -p office


cd apple && rm -rf /etc/fonts/apple/*

curl -LOs 'https://raw.githubusercontent.com/posteroffonts/sanfran/master/System/Library/Fonts/SFNS.ttf'
curl -LOs 'https://raw.githubusercontent.com/posteroffonts/sanfran/master/System/Library/Fonts/SFNSItalic.ttf'
curl -LOs 'https://raw.githubusercontent.com/posteroffonts/sanfran/master/System/Library/Fonts/SFNSMono.ttf'
curl -LOs 'https://raw.githubusercontent.com/posteroffonts/sanfran/master/System/Library/Fonts/SFNSMonoItalic.ttf'

curl -LOs 'https://raw.githubusercontent.com/posteroffonts/sanfran/master/SF-Mono/SF-Mono-Regular.otf'
curl -LOs 'https://raw.githubusercontent.com/posteroffonts/sanfran/master/SF-Mono/SF-Mono-RegularItalic.otf'
curl -LOs 'https://raw.githubusercontent.com/posteroffonts/sanfran/master/SF-Mono/SF-Mono-Medium.otf'
curl -LOs 'https://raw.githubusercontent.com/posteroffonts/sanfran/master/SF-Mono/SF-Mono-MediumItalic.otf'
curl -LOs 'https://raw.githubusercontent.com/posteroffonts/sanfran/master/SF-Mono/SF-Mono-Bold.otf'
curl -LOs 'https://raw.githubusercontent.com/posteroffonts/sanfran/master/SF-Mono/SF-Mono-BoldItalic.otf'

curl -LOs 'https://raw.githubusercontent.com/posteroffonts/sanfran/master/SF-Pro/SF-Pro-Text-Light.otf'
curl -LOs 'https://raw.githubusercontent.com/posteroffonts/sanfran/master/SF-Pro/SF-Pro-Text-LightItalic.otf'
curl -LOs 'https://raw.githubusercontent.com/posteroffonts/sanfran/master/SF-Pro/SF-Pro-Text-Regular.otf'
curl -LOs 'https://raw.githubusercontent.com/posteroffonts/sanfran/master/SF-Pro/SF-Pro-Text-RegularItalic.otf'
curl -LOs 'https://raw.githubusercontent.com/posteroffonts/sanfran/master/SF-Pro/SF-Pro-Text-Medium.otf'
curl -LOs 'https://raw.githubusercontent.com/posteroffonts/sanfran/master/SF-Pro/SF-Pro-Text-MediumItalic.otf'
curl -LOs 'https://raw.githubusercontent.com/posteroffonts/sanfran/master/SF-Pro/SF-Pro-Text-Bold.otf'
curl -LOs 'https://raw.githubusercontent.com/posteroffonts/sanfran/master/SF-Pro/SF-Pro-Text-BoldItalic.otf'

curl -LOs 'https://raw.githubusercontent.com/posteroffonts/sanfran/master/System/Library/Fonts/ArabicUIText.ttc'
curl -LOs 'https://raw.githubusercontent.com/posteroffonts/sanfran/master/System/Library/Fonts/ArabicUIDisplay.ttc'
# curl -LOs 'https://raw.githubusercontent.com/posteroffonts/sanfran/master/New%20York/NewYorkSmall-Regular.otf'
# curl -LOs 'https://raw.githubusercontent.com/posteroffonts/sanfran/master/New%20York/NewYorkSmall-RegularItalic.otf'
# curl -LOs 'https://raw.githubusercontent.com/posteroffonts/sanfran/master/New%20York/NewYorkSmall-Bold.otf'
# curl -LOs 'https://raw.githubusercontent.com/posteroffonts/sanfran/master/New%20York/NewYorkSmall-BoldItalic.otf'
# curl -LOs 'https://raw.githubusercontent.com/posteroffonts/sanfran/master/New%20York/NewYorkMedium-Regular.otf'
# curl -LOs 'https://raw.githubusercontent.com/posteroffonts/sanfran/master/New%20York/NewYorkMedium-RegularItalic.otf'
# curl -LOs 'https://raw.githubusercontent.com/posteroffonts/sanfran/master/New%20York/NewYorkMedium-Bold.otf'
# curl -LOs 'https://raw.githubusercontent.com/posteroffonts/sanfran/master/New%20York/NewYorkMedium-BoldItalic.otf'
# curl -LOs 'https://raw.githubusercontent.com/posteroffonts/sanfran/master/New%20York/NewYorkLarge-Regular.otf'
# curl -LOs 'https://raw.githubusercontent.com/posteroffonts/sanfran/master/New%20York/NewYorkLarge-RegularItalic.otf'
# curl -LOs 'https://raw.githubusercontent.com/posteroffonts/sanfran/master/New%20York/NewYorkLarge-Bold.otf'
# curl -LOs 'https://raw.githubusercontent.com/posteroffonts/sanfran/master/New%20York/NewYorkLarge-BoldItalic.otf'
# curl -LOs 'https://raw.githubusercontent.com/posteroffonts/sanfran/master/New%20York/NewYorkExtraLarge-Regular.otf'
# curl -LOs 'https://raw.githubusercontent.com/posteroffonts/sanfran/master/New%20York/NewYorkExtraLarge-RegularItalic.otf'
# curl -LOs 'https://raw.githubusercontent.com/posteroffonts/sanfran/master/New%20York/NewYorkExtraLarge-Bold.otf'
# curl -LOs 'https://raw.githubusercontent.com/posteroffonts/sanfran/master/New%20York/NewYorkExtraLarge-BoldItalic.otf'
# curl -LOs 'https://raw.githubusercontent.com/posteroffonts/sanfran/master/System/Library/Fonts/PingFang.ttc'
curl -LOs 'https://raw.githubusercontent.com/posteroffonts/sanfran/master/com_apple_MobileAsset_Font6/Hiragino_Sans_CNS.ttc'
curl  -Ls 'https://raw.githubusercontent.com/posteroffonts/sanfran/master/System/Library/Fonts/Hiragino%20Sans%20GB.ttc' -o 'Hiragino_Sans_GB.ttc' # actually spaces but nobody likes spaces
curl  -Ls 'https://raw.githubusercontent.com/posteroffonts/sanfran/master/System/Library/Fonts/%E3%83%92%E3%83%A9%E3%82%AE%E3%83%8E%E8%A7%92%E3%82%B4%E3%82%B7%E3%83%83%E3%82%AF%20W3.ttc' -o 'ヒラギノ角ゴシック W3.ttc'
curl  -Ls 'https://raw.githubusercontent.com/posteroffonts/sanfran/master/System/Library/Fonts/%E3%83%92%E3%83%A9%E3%82%AE%E3%83%8E%E8%A7%92%E3%82%B4%E3%82%B7%E3%83%83%E3%82%AF%20W6.ttc' -o 'ヒラギノ角ゴシック W6.ttc'
curl -LOs 'https://raw.githubusercontent.com/posteroffonts/sanfran/master/System/Library/Fonts/Supplemental/AppleGothic.ttf'
# curl  -Ls 'https://raw.githubusercontent.com/posteroffonts/sanfran/master/System/Library/Fonts/STHeiti%20Light.ttc' -o 'STHeiti Light.ttc' 
# curl  -Ls 'https://raw.githubusercontent.com/posteroffonts/sanfran/master/System/Library/Fonts/STHeiti%20Medium.ttc' -o 'STHeiti Medium.ttc'
# curl -LOs 'https://raw.githubusercontent.com/posteroffonts/sanfran/master/com_apple_MobileAsset_Font6/Hannotate.ttc'
# curl -LOs 'https://raw.githubusercontent.com/posteroffonts/sanfran/master/com_apple_MobileAsset_Font6/HanziPen.ttc'
# curl -LOs 'https://raw.githubusercontent.com/posteroffonts/sanfran/master/com_apple_MobileAsset_Font6/Libian.ttc'
# curl -LOs 'https://raw.githubusercontent.com/posteroffonts/sanfran/master/com_apple_MobileAsset_Font6/Baoli.ttc'
curl -LOs 'https://raw.githubusercontent.com/posteroffonts/sanfran/master/com_apple_MobileAsset_Font6/Kaiti.ttc.txz' && tar xvf Kaiti.ttc.txz && rm Kaiti.ttc.txz
curl -LOs 'https://raw.githubusercontent.com/posteroffonts/sanfran/master/com_apple_MobileAsset_Font6/Xingkai.ttc'

cd ..


cd office && rm -rf /etc/fonts/office/*

# curl -LOs 'https://raw.githubusercontent.com/posteroffonts/sanfran/master/Microsoft%20Office/Deng.ttf'
# curl -LOs 'https://raw.githubusercontent.com/posteroffonts/sanfran/master/Microsoft%20Office/Dengb.ttf'
# curl -LOs 'https://raw.githubusercontent.com/posteroffonts/sanfran/master/Microsoft%20Office/Dengl.ttf'
curl -LOs 'https://raw.githubusercontent.com/posteroffonts/sanfran/master/Microsoft%20Office/Consola.ttf'
curl -LOs 'https://raw.githubusercontent.com/posteroffonts/sanfran/master/Microsoft%20Office/Consolai.ttf'
curl -LOs 'https://raw.githubusercontent.com/posteroffonts/sanfran/master/Microsoft%20Office/Consolab.ttf'
curl -LOs 'https://raw.githubusercontent.com/posteroffonts/sanfran/master/Microsoft%20Office/Consolaz.ttf'
curl -LOs 'https://raw.githubusercontent.com/posteroffonts/sanfran/master/Microsoft%20Office/times.ttf'
curl -LOs 'https://raw.githubusercontent.com/posteroffonts/sanfran/master/Microsoft%20Office/timesi.ttf'
curl -LOs 'https://raw.githubusercontent.com/posteroffonts/sanfran/master/Microsoft%20Office/timesbd.ttf'
curl -LOs 'https://raw.githubusercontent.com/posteroffonts/sanfran/master/Microsoft%20Office/timesbi.ttf'
curl -LOs 'https://raw.githubusercontent.com/posteroffonts/sanfran/master/Microsoft%20Office/SabonNextLT.ttf'
curl -LOs 'https://raw.githubusercontent.com/posteroffonts/sanfran/master/Microsoft%20Office/SabonNextLT-Italic.ttf'
curl -LOs 'https://raw.githubusercontent.com/posteroffonts/sanfran/master/Microsoft%20Office/SabonNextLT-Bold.ttf'
curl -LOs 'https://raw.githubusercontent.com/posteroffonts/sanfran/master/Microsoft%20Office/SabonNextLT-BoldItalic.ttf'

cd ..
