#!/bin/sh -v

column -t -s : /etc/passwd | sort -r -h -k 3 | awk '{print $1}' | head -n 1
