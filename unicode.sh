#!/bin/sh -v

cd /tmp

[ -e /tmp/Blocks.txt ] ||
curl -O "https://unicode.org/Public/UCD/latest/ucd/Blocks.txt"

: ${COLS:=16}

: ${PLAIN:=/tmp/unicode}
: ${TEX:=/tmp/unicode.tex}
: ${HTML:=/tmp/unicode.html}

PRINTF() {
	/usr/bin/printf "${@}"
}

BLOCKS() {
	cat /tmp/Blocks.txt |
		sed -E -e '/^[[:space:]]*#/d' -e '/^$/d' |
		cut -d ';' -f 2 |
		sed -E -e 's/^[[:space:]]*//g' |
		grep -E -v -e 'Private Use Area' -e '^Tags$' -e 'Variation Selectors' -e 'Surrogates'
}

BLOCK() {
	grep -E "^[0-9A-Fa-f]{1,8}\.\.[0-9A-Fa-f]{1,8}; ${SECTION}$" Blocks.txt
}

START() {
	BLOCK | grep -E -o     "[0-9A-F]+\.\." | tr -d .
}
END() {
	BLOCK | grep -E -o "\.\.[0-9A-F]+"     | tr -d .
}

cat << EOF >> ${HTML}
<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8" />
    <style>
html body {
  font-synthesis: none;
  font-family: serif;
  font-size: 1.5rem;
  line-height: 2rem;
  text-align: center;
}
td, th {
  max-width: 2rem;
}
th {
  padding-top: 2rem;
}
html body table tbody * {
  font-size-adjust: none !important;
}
table {
  margin-left: auto;
  margin-right: auto;
}
    </style>
  </head>
  <body>
EOF

echo "    <table>" >> ${HTML}

IFS='
'
for SECTION in $(BLOCKS); do
	echo "      <thead>" >> ${HTML}
	echo "        <tr>\n          <th colspan=\"${COLS}\">${SECTION}</th>\n        </tr>" >> ${HTML}
	echo "      </thead>" >> ${HTML}
	echo "      <tbody id=\"$(PRINTF "${SECTION}" | sed 's/ /-/g')\">" >> ${HTML}
	echo "\n\n${SECTION}\n" >> ${PLAIN}
	echo "\n\n${SECTION}\n" >> ${TEX}
	for ROW in $(seq $(PRINTF "%d" 0x$(START)) ${COLS} $(PRINTF "%d" 0x$(END))); do
		echo "        <tr>" >> ${HTML}
		for COL in $(seq ${ROW} $((${ROW}+${COLS}-1))); do
			echo -n "          <td>" >> ${HTML}
			PRINTF "&#x$(PRINTF "%0.8X" ${COL});" &
			PRINTF '\U'"$(PRINTF "%0.8X" ${COL})" >> ${PLAIN} &
			PRINTF "\\symbol{\"$(PRINTF "%0.8X" ${COL})}" >> ${TEX} &
			wait
			echo "</td>" >> ${HTML}
		done
		echo "        </tr>" >> ${HTML}
		echo >> ${PLAIN}
		echo >> ${TEX}
	done >> ${HTML}
	echo "      </tbody>" >> ${HTML}
done

echo "    </table>" >> ${HTML}

cat << EOF >> ${HTML}
  </body>
</html>
EOF

cp ${PLAIN} /var/tmp/unicode
cp ${TEX} /var/tmp/unicode.tex
cp ${HTML}  /var/tmp/unicode.html
