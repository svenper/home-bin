#!/bin/sh -v

cat << EOF | sudo tee /etc/resolv.conf.head
nameserver 2001:4860:4860::8888
nameserver 2001:4860:4860::8844
nameserver 8.8.8.8
nameserver 8.8.4.4
EOF
