#!/bin/sh -v

rm -rf --interactive=once ${HOME}/void-glibc
mkdir -p ${HOME}/void-glibc
chown ${USER}:${USER} ${HOME}/void-glibc

cd ${HOME}/void-glibc

XBPS_ARCH=x86_64 xbps-install -S --repository=https://alpha.de.repo.voidlinux.org/current -r ${HOME}/void-glibc base-voidstrap zsh fontconfig libX11 libXft perl git vim xtools proot gdb p7zip tmux st-terminfo

cp /etc/resolv.conf ${HOME}/void-glibc/etc/
cp -r /etc/sudoers.d/* ${HOME}/void-glibc/etc/sudoers.d/
cp -r /etc/fonts/* ${HOME}/void-glibc/etc/fonts/

mkdir -p          ${HOME}/void-glibc/var/cache/xbps-src/binpkgs ${HOME}/void-glibc/var/cache/xbps-src/repocache ${HOME}/void-glibc/var/cache/xbps-src/sources
chown ${USER}:${USER} ${HOME}/void-glibc/var/cache/xbps-src/binpkgs ${HOME}/void-glibc/var/cache/xbps-src/repocache ${HOME}/void-glibc/var/cache/xbps-src/sources
chroot ${HOME}/void-glibc useradd -m -s /bin/zsh -U -G wheel,users,audio,video,cdrom,input,xbuilder ${USER}

echo -n 'root: '
chroot ${HOME}/void-glibc passwd root
echo -n ${USER}': '
chroot ${HOME}/void-glibc passwd ${USER}

rm -rf ${HOME}/void-glibc/${HOME}/.*
cp -r ${HOME}/bin ${HOME}/.dotfiles/.profile ${HOME}/.dotfiles/.vim ${HOME}/.dotfiles/.vimrc ${HOME}/.dotfiles/.zshrc ${HOME}/.zhistory ${HOME}/void-glibc/${HOME}/
chown -R ${USER}:${USER} ${HOME}/void-glibc/${HOME}

find ${HOME}/void-glibc/${HOME} -name .git -exec rm -rf {} \;
