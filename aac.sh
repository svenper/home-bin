#!/bin/sh -v

cd ~/Music
for FILE in */*/*.m4a
do
    DIR=$(dirname "$FILE")
    mkdir -p "/var/tmp/$DIR"
    [ -e "/var/tmp/$FILE" ] && continue
    ffmpeg                     \
        -i "$FILE"             \
        -c:a libfdk_aac        \ 
        -vbr 8                 \
        "/var/tmp/$FILE"
done
