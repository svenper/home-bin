#!/bin/sh

defaults write com.apple.desktopservices DSDontWriteNetworkStores -bool true
defaults write com.apple.universalaccess mouseDriverCursorSize -float 1.2
defaults write com.apple.screencapture disable-shadow -bool true
defaults write com.apple.iTunes DeviceBackupsDisabled -bool true

git config --global core.precomposeunicode true
#git config --local core.precomposeunicode true
