#!/bin/sh -v

[ -e "$1" ] || (echo "no such file"; exit 1)

ORIG_OWNER=$(stat -c "%u:%g" "$1")
ORIG_PERMS=$(stat -c "%a" "$1")
ID="$(head -c 4 /dev/urandom | xxd -p)"
FILE="/var/tmp/$(basename "$1").$ID"

doas cp "$1" "$FILE" &&
doas chmod 600 "$FILE" &&
doas chown $(whoami):$(whoami) "$FILE" &&
${EDITOR:-vim} "$FILE" &&
doas cp "$FILE" "$1.$ID" &&
doas chown $ORIG_OWNER "$1.$ID" &&
doas chmod $ORIG_PERMS "$1.$ID" &&
doas mv "$1.$ID" "$1"
