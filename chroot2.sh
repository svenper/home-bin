#!/bin/sh -v

sudo lsblk -o NAME,FSTYPE,LABEL

read -p "Enter boot partition /boot (e.g. sda1): " BOOT
read -p "Enter root partition /     (e.g. sda3): " ROOT

sudo mount /dev/"${ROOT}" /mnt
sudo mount /dev/"${BOOT}" /mnt/boot

sudo mount -t proc proc /mnt/proc
sudo mount -t sysfs sys /mnt/sys
sudo mount -o bind /dev /mnt/dev
sudo mount -t devpts pts /mnt/dev/pts
sudo mount -o bind /tmp /mnt/tmp

cd /mnt
cp /etc/resolv.conf /mnt/etc/resolv.conf
cp /*.sh /mnt/tmp
PS1='CHROOT$ ' sudo chroot /mnt /bin/zsh

sudo umount /mnt/tmp
sudo umount /mnt/dev/pts
sudo umount /mnt/dev
sudo umount /mnt/sys
sudo umount /mnt/proc

sudo umount /mnt/boot
sudo umount /mnt
