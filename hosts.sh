#!/bin/sh -v

LC_ALL=en_US.UTF-8
export LC_ALL

cd /tmp
rm -rf		hosts-franken
mkdir		hosts-franken
cd		hosts-franken

for FILE in	"http://winhelp2002.mvps.org/hosts.txt" \
		"http://www.malwaredomainlist.com/hostslist/hosts.txt" \
		"https://adaway.org/hosts.txt" \
		"https://someonewhocares.org/hosts/zero/hosts" \
		"https://pgl.yoyo.org/adservers/serverlist.php?hostformat=hosts&mimetype=plaintext&useip=0.0.0.0" \
		"https://raw.githubusercontent.com/mitchellkrogza/Badd-Boyz-Hosts/master/hosts" \
		"https://raw.githubusercontent.com/Sinfonietta/hostfiles/master/gambling-hosts" \
		"https://raw.githubusercontent.com/Sinfonietta/hostfiles/master/pornography-hosts" \
		"https://raw.githubusercontent.com/Clefspeare13/pornhosts/master/0.0.0.0/hosts"
	do	curl -Ls -o "$(echo -n ${FILE} | shasum -a 256 | cut -d ' ' -f 1)" "${FILE}" &
done

wait

find . ! -iname hosts.tmp -type f -exec cat {} \; >> hosts.tmp.online

cat hosts.tmp.online |
	tr -d '\r' |
	sed '/::/d' |
	sed '/^[[:space:]]*#/d' |
	sed -E 's/[[:space:]]+/\t/g' |
	sed -E 's/\t#.*//g' |
	sed -E 's/\t$//g' |
	sed 's/^127\.0\.0\.1/0.0.0.0/g' |
	sed '/localhost/d' |
	sed '/255.255.255.255/d' |
	sed -E '/[[:space:]](www.)?thepiratebay\.[a-z]+$/d' |
	sed '/^$/d' |
	sort |
	uniq |
	grep -ax '.*' | # delete lines with invalid characters
	tee -a hosts.tmp 1>/dev/null

echo -n "Press enter to continue with install: "
read THIS_VARIABLE_ONLY_FOR_PRESSING_ENTER

sudo sed -i '/^0\.0\.0\.0[[:space:]]/d' /etc/hosts

cat hosts.tmp | sudo tee -a /etc/hosts 1>/dev/null
