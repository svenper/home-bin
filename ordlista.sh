#!/bin/sh -v

DIR="$(mktemp -d /tmp/ordlista-XXXXXX)"
cd "${DIR}"

curl -O 'http://www.csc.kth.se/~karlan/AoK/Labb1/ordlista.txt'
curl -O 'https://www.it.uu.se/edu/course/homepage/oopjava/vt15/program/ordlista/svenska.txt'
curl -O 'http://runeberg.org/words/ss100.txt'
curl -O 'http://runeberg.org/words/ord.gatunamn.posten'
curl -O 'http://runeberg.org/words/ord.ortsnamn.posten'
curl -O 'http://runeberg.org/words/ord.niklas.frykholm'
curl 'http://www.csc.kth.se/tcs/humanlang/tools/GPLstava_lib.tgz' | tar -xvzf - --strip-components=1 --no-anchored --wildcards 'datatermer*' forkortningar namn tex

rm total.txt final.txt
for DICTFILE in *; do
        CHARSET=$(file -bi "${DICTFILE}" | awk '{print $2}' | sed 's|.*=||g')
        printf "${DICTFILE}: ${CHARSET}\n"
        iconv -f "${CHARSET}" -t UTF-8 < "${DICTFILE}" | tr -d '\r' >> total.txt
done
sort -u total.txt > final.txt

cp final.txt ${HOME}/.local/share/nvim/site/spell/sv.utf-8.add
vim -c 'mkspell! '"${HOME}"'/.local/share/nvim/site/spell/sv.utf-8.add|q'
