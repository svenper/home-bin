#!/bin/sh -v

[ "$PWD" = "$HOME" ] && exit
[ -x "$(command -v cueprint)" ] || exit 

printf "artist: "
read ARTIST
printf "album: "
read ALBUM
printf "date: "
read DATE

TRACK=1
for FILE in *.wav
do
    printf "$(printf %02d $TRACK): title: "
    read TITLE$TRACK
    TRACK=$(($TRACK+1))
done

TRACK=1
for FILE in *.wav
do
    ffmpeg -loglevel warning -y    \
        -i "$FILE"                 \
	-movflags +faststart       \
	-metadata title="$(eval printf \"\$TITLE$TRACK\")" \
        -metadata artist="$ARTIST" \
        -metadata date="$DATE"     \
        -metadata track="$TRACK"   \
        -metadata album="$ALBUM"   \
        -acodec alac               \
	"$(printf %02d $TRACK) $(printf "$(eval printf \"\$TITLE$TRACK\")" | tr '.~/!?' '_').m4a"
    TRACK=$(($TRACK+1))
done

[ -e audio.jpg ] && IMAGE_EXT="jpg"
[ -e audio.jpeg ] && IMAGE_EXT="jpeg"
[ -e audio.png ] && IMAGE_EXT="png"
find . -name "*.m4a" -exec AtomicParsley {} --albumArtist "${ARTIST}" --artwork "audio.${IMAGE_EXT}" --overWrite \;


mkdir -p "$ARTIST/$ALBUM"
mv *.m4a "$ARTIST/$ALBUM"
