#!/bin/sh

cd "${1:-.}" || exit 1

FIRST="$(find . -iname "01 *.m4a" -o -iname "1-01 *.m4a" -o -iname "* - * - 01 *.m4a" -o -iname "* - * - 1-01 *.m4a")"
AUDIO_ALBUM_ARTIST="$(ffprobe "${FIRST}" 2>&1 | grep -Po '^[[:space:]]+(album_)?artist[[:space:]]+:[[:space:]]+\K.*' | sort -h | head -1)"
AUDIO_ALBUM="$(       ffprobe "${FIRST}" 2>&1 | grep -Po '^[[:space:]]+album[[:space:]]+:[[:space:]]+\K.*'                     | head -1)"
AUDIO_ALBUM_ARTIST_SAFE="$(echo "${AUDIO_ALBUM_ARTIST}" | tr '.~/!?' '_')"
AUDIO_ALBUM_SAFE="$(echo "${AUDIO_ALBUM}" | tr '.~/!?' '_')"
mkdir -p /tmp/"${AUDIO_ALBUM_ARTIST_SAFE:=ARTIST}"/"${AUDIO_ALBUM_SAFE:=ALBUM}"
for AUDIO_FILE in *.m4a; do
	AUDIO_ARTIST="$(ffprobe "${AUDIO_FILE}" 2>&1 | grep -Po '^[[:space:]]+artist[[:space:]]+:[[:space:]]+\K.*' | head -1)"
	AUDIO_DATE="$(  ffprobe "${AUDIO_FILE}" 2>&1 | grep -Po '^[[:space:]]+date[[:space:]]+:[[:space:]]+\K.*'   | head -1)"
	AUDIO_DISC="$(  ffprobe "${AUDIO_FILE}" 2>&1 | grep -Po '^[[:space:]]+disc[[:space:]]+:[[:space:]]+\K.*'   | head -1)"
	AUDIO_TRACK="$( ffprobe "${AUDIO_FILE}" 2>&1 | grep -Po '^[[:space:]]+track[[:space:]]+:[[:space:]]+\K.*'  | head -1)"
	AUDIO_TITLE="$( ffprobe "${AUDIO_FILE}" 2>&1 | grep -Po '^[[:space:]]+title[[:space:]]+:[[:space:]]+\K.*'  | head -1)"
	AtomicParsley "${AUDIO_FILE}" -E
	ffmpeg -i "${AUDIO_FILE}" -f wav -ar 44100 -sample_fmt s16 -dither_method improved_e_weighted -map_metadata -1 - |
		fdkaac -b256 -m5 -p2 -a1 -f0 -C -G2 --moov-before-mdat -o /tmp/"${AUDIO_FILE}" - 
	AtomicParsley /tmp/"${AUDIO_FILE}" \
		--artist      "${AUDIO_ARTIST}" \
		--albumArtist "${AUDIO_ALBUM_ARTIST}" \
		--album       "${AUDIO_ALBUM}" \
		--year        "${AUDIO_DATE}" \
		--tracknum    "${AUDIO_TRACK}" \
		--disk        "${AUDIO_DISC}" \
		--title       "${AUDIO_TITLE}" \
		--artwork     "${AUDIO_FILE%.m4a}_artwork_1".*g \
		--gapless     true \
		--overWrite
	mv /tmp/"${AUDIO_FILE}" /tmp/"${AUDIO_ALBUM_ARTIST_SAFE}"/"${AUDIO_ALBUM_SAFE}"/
done

adb push /tmp/"${AUDIO_ALBUM_ARTIST_SAFE}" /sdcard/Music/

rm -r *_artwork_[0-9].*g /tmp/"${AUDIO_ALBUM_ARTIST_SAFE}"

# android xargs is stupid, doesn't understand that file:// and {} are different things
adb shell find \"/sdcard/Music/"${AUDIO_ALBUM_ARTIST_SAFE}/${AUDIO_ALBUM_SAFE}/"\" -type f | sed -E -e "s/'/'\\\''/g" -e 's/^|$/"/g' -e 's/([ ()])/\\\1/g' |
	xargs -I{} adb shell am broadcast -a android.intent.action.MEDIA_SCANNER_SCAN_FILE -d file://{}
