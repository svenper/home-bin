#!/bin/sh -v

NOTO="Noto-unhinted.zip"
GENTIUM="GentiumPlus-5.000.zip"
GENTIUMBASIC="GentiumBasic_1102.zip"
ANDIKA="Andika-5.000.zip"
ANDIKANEWBASIC="AndikaNewBasic-5.500.zip"
CHARIS="CharisSIL-5.000.zip"
AWAMI="AwamiNastaliq-2.000.zip"
ROBOTO="roboto-unhinted.zip"
INPUT="Input-Font.zip"
COELACANTH_VERSION="0.006"
COELACANTH="coelacanth_v${COELACANTH_VERSION}.zip"
GFSDIDOT="GFS_Didot_Classic.zip"
GFSNEOHELLENIC="GFS_NeoHellenic.zip"
GFSBASKERVILLE="GFS_Baskerville.zip"
GFSELPIS="GFS_Elpis.zip"
VOLLKORN="vollkorn-4-105.zip"
BRILL="static_typefacedownload_typefacepackage.zip"

DIR="/tmp/global"

rm -rf ${DIR}
mkdir ${DIR}
cd ${DIR}

github_fetch() {
	REPO="${1}"
	VERSION="${2}"
	FILENAME_EXC="${3}"
	curl -LOs "$(
		curl -Ls "https://api.github.com/repos/${REPO}/releases/${VERSION}" |
			grep browser_download_url |
			grep ${FILENAME_EXC} |
			cut -d '"' -f 4
	)" || exit 1
	return 0
}

github_fetch "google/roboto" "latest" "roboto-unhinted\.zip" || exit 1

github_fetch "be5invis/Iosevka" "latest" "iosevka-term-slab-.*\.zip" || exit 1

github_fetch "be5invis/Iosevka" "latest" "iosevka-term-ss08-.*\.zip" || exit 1

github_fetch "rsms/inter" "latest" "Inter.*\.zip" || exit 1

#github_fetch "Fuzzypeg/Coelacanth" "latest" "coelacanth.*\.zip"
curl -LOs "https://github.com/Fuzzypeg/Coelacanth/releases/download/v${COELACANTH_VERSION}/coelacanth_v${COELACANTH_VERSION}.zip" || exit 1

### TODO: make these (or all) download in background but still fail noisily (function nesting?)

#curl -LOs "https://www.cns11643.gov.tw/AIDB/Open_Data.zip" &
#curl -LOs "https://www.cns11643.gov.tw/download/%E5%AD%97%E5%9E%8B%E4%B8%8B%E8%BC%89%601q%60%E8%AA%AA%E6%96%87%E8%A7%A3%E5%AD%97True%60w%60Type%E5%AD%97%E5%9E%8B/name/ebas927.ttf" &

#curl -LOs "https://noto-website-2.storage.googleapis.com/pkgs/${NOTO}" &

# curl -LOs "https://www.unicode.org/cgi-bin/LastResortDownload.zip" || exit 1

# Adobe NotDef
curl -LOs "https://raw.githubusercontent.com/adobe-fonts/adobe-notdef/master/AND-Regular.otf" || exit 1

curl -LOs "https://raw.githubusercontent.com/adobe-fonts/adobe-blank/master/AdobeBlank.otf" || exit 1
curl -LOs "https://raw.githubusercontent.com/adobe-fonts/adobe-blank-2/master/AdobeBlank2.otf" || exit 1

curl -LOs "https://raw.githubusercontent.com/adobe-fonts/locl-test/master/LOCLTest-Regular.otf" || exit 1

curl -LOs "https://raw.githubusercontent.com/hfg-gmuend/openmoji/master/font/OpenMoji-Color.ttf" || exit 1

curl -LOs "https://raw.githubusercontent.com/adobe-fonts/source-han-serif/release/OTF/SimplifiedChinese/SourceHanSerifSC-ExtraLight.otf" || exit 1
curl -LOs "https://raw.githubusercontent.com/adobe-fonts/source-han-serif/release/OTF/SimplifiedChinese/SourceHanSerifSC-Light.otf" || exit 1
curl -LOs "https://raw.githubusercontent.com/adobe-fonts/source-han-serif/release/OTF/SimplifiedChinese/SourceHanSerifSC-Regular.otf" || exit 1
curl -LOs "https://raw.githubusercontent.com/adobe-fonts/source-han-serif/release/OTF/SimplifiedChinese/SourceHanSerifSC-Medium.otf" || exit 1
curl -LOs "https://raw.githubusercontent.com/adobe-fonts/source-han-serif/release/OTF/SimplifiedChinese/SourceHanSerifSC-SemiBold.otf" || exit 1
curl -LOs "https://raw.githubusercontent.com/adobe-fonts/source-han-serif/release/OTF/SimplifiedChinese/SourceHanSerifSC-Bold.otf" || exit 1
curl -LOs "https://raw.githubusercontent.com/adobe-fonts/source-han-serif/release/OTF/SimplifiedChinese/SourceHanSerifSC-Heavy.otf" || exit 1

curl -LOs "https://raw.githubusercontent.com/adobe-fonts/source-han-sans/release/OTF/SimplifiedChinese/SourceHanSansSC-ExtraLight.otf" || exit 1
curl -LOs "https://raw.githubusercontent.com/adobe-fonts/source-han-sans/release/OTF/SimplifiedChinese/SourceHanSansSC-Light.otf" || exit 1
curl -LOs "https://raw.githubusercontent.com/adobe-fonts/source-han-sans/release/OTF/SimplifiedChinese/SourceHanSansSC-Normal.otf" || exit 1
curl -LOs "https://raw.githubusercontent.com/adobe-fonts/source-han-sans/release/OTF/SimplifiedChinese/SourceHanSansSC-Regular.otf" || exit 1
curl -LOs "https://raw.githubusercontent.com/adobe-fonts/source-han-sans/release/OTF/SimplifiedChinese/SourceHanSansSC-Medium.otf" || exit 1
curl -LOs "https://raw.githubusercontent.com/adobe-fonts/source-han-sans/release/OTF/SimplifiedChinese/SourceHanSansSC-Bold.otf" || exit 1
curl -LOs "https://raw.githubusercontent.com/adobe-fonts/source-han-sans/release/OTF/SimplifiedChinese/SourceHanSansSC-Heavy.otf" || exit 1

curl -LOs "https://raw.githubusercontent.com/adobe-fonts/source-han-mono/master/Normal/OTC/SourceHanMonoSC-Normal.otf" || exit 1
curl -LOs "https://raw.githubusercontent.com/adobe-fonts/source-han-mono/master/Bold/OTC/SourceHanMonoSC-Bold.otf" || exit 1

curl -LOs "https://raw.githubusercontent.com/adobe-fonts/source-serif-pro/release/OTF/SourceSerifPro-Light.otf" || exit 1
curl -LOs "https://raw.githubusercontent.com/adobe-fonts/source-serif-pro/release/OTF/SourceSerifPro-LightIt.otf" || exit 1
curl -LOs "https://raw.githubusercontent.com/adobe-fonts/source-serif-pro/release/OTF/SourceSerifPro-Regular.otf" || exit 1
curl -LOs "https://raw.githubusercontent.com/adobe-fonts/source-serif-pro/release/OTF/SourceSerifPro-It.otf" || exit 1
curl -LOs "https://raw.githubusercontent.com/adobe-fonts/source-serif-pro/release/OTF/SourceSerifPro-Bold.otf" || exit 1
curl -LOs "https://raw.githubusercontent.com/adobe-fonts/source-serif-pro/release/OTF/SourceSerifPro-BoldIt.otf" || exit 1

curl -LOs "https://raw.githubusercontent.com/adobe-fonts/source-sans-pro/release/OTF/SourceSans3-Light.otf" || exit 1
curl -LOs "https://raw.githubusercontent.com/adobe-fonts/source-sans-pro/release/OTF/SourceSans3-LightIt.otf" || exit 1
curl -LOs "https://raw.githubusercontent.com/adobe-fonts/source-sans-pro/release/OTF/SourceSans3-Regular.otf" || exit 1
curl -LOs "https://raw.githubusercontent.com/adobe-fonts/source-sans-pro/release/OTF/SourceSans3-It.otf" || exit 1
curl -LOs "https://raw.githubusercontent.com/adobe-fonts/source-sans-pro/release/OTF/SourceSans3-Bold.otf" || exit 1
curl -LOs "https://raw.githubusercontent.com/adobe-fonts/source-sans-pro/release/OTF/SourceSans3-BoldIt.otf" || exit 1

curl -LOs "https://raw.githubusercontent.com/adobe-fonts/source-code-pro/release/OTF/SourceCodePro-Light.otf" || exit 1
curl -LOs "https://raw.githubusercontent.com/adobe-fonts/source-code-pro/release/OTF/SourceCodePro-LightIt.otf" || exit 1
curl -LOs "https://raw.githubusercontent.com/adobe-fonts/source-code-pro/release/OTF/SourceCodePro-Regular.otf" || exit 1
curl -LOs "https://raw.githubusercontent.com/adobe-fonts/source-code-pro/release/OTF/SourceCodePro-It.otf" || exit 1
curl -LOs "https://raw.githubusercontent.com/adobe-fonts/source-code-pro/release/OTF/SourceCodePro-Bold.otf" || exit 1
curl -LOs "https://raw.githubusercontent.com/adobe-fonts/source-code-pro/release/OTF/SourceCodePro-BoldIt.otf" || exit 1

curl -LOs "https://raw.githubusercontent.com/google/fonts/master/apache/robotomono/RobotoMono-Regular.ttf" || exit 1
curl -LOs "https://raw.githubusercontent.com/google/fonts/master/apache/robotomono/RobotoMono-Italic.ttf" || exit 1
curl -LOs "https://raw.githubusercontent.com/google/fonts/master/apache/robotomono/RobotoMono-Bold.ttf" || exit 1
curl -LOs "https://raw.githubusercontent.com/google/fonts/master/apache/robotomono/RobotoMono-BoldItalic.ttf" || exit 1

curl -LOs "https://raw.githubusercontent.com/google/fonts/master/ofl/spacemono/SpaceMono-Regular.ttf" || exit 1
curl -LOs "https://raw.githubusercontent.com/google/fonts/master/ofl/spacemono/SpaceMono-Italic.ttf" || exit 1
curl -LOs "https://raw.githubusercontent.com/google/fonts/master/ofl/spacemono/SpaceMono-Bold.ttf" || exit 1
curl -LOs "https://raw.githubusercontent.com/google/fonts/master/ofl/spacemono/SpaceMono-BoldItalic.ttf" || exit 1

curl -LOs "https://raw.githubusercontent.com/jpt/barlow/master/fonts/ttf/Barlow-Regular.ttf" || exit 1
curl -LOs "https://raw.githubusercontent.com/jpt/barlow/master/fonts/ttf/Barlow-Italic.ttf" || exit 1
curl -LOs "https://raw.githubusercontent.com/jpt/barlow/master/fonts/ttf/Barlow-Bold.ttf" || exit 1
curl -LOs "https://raw.githubusercontent.com/jpt/barlow/master/fonts/ttf/Barlow-BoldItalic.ttf" || exit 1

curl -LOs "https://raw.githubusercontent.com/jpt/barlow/master/fonts/ttf/BarlowSemiCondensed-Regular.ttf" || exit r
curl -LOs "https://raw.githubusercontent.com/jpt/barlow/master/fonts/ttf/BarlowSemiCondensed-Italic.ttf" || exit 1
curl -LOs "https://raw.githubusercontent.com/jpt/barlow/master/fonts/ttf/BarlowSemiCondensed-Bold.ttf" || exit 1
curl -LOs "https://raw.githubusercontent.com/jpt/barlow/master/fonts/ttf/BarlowSemiCondensed-BoldItalic.ttf" || exit 1

curl -LOs "https://raw.githubusercontent.com/jpt/barlow/master/fonts/ttf/BarlowCondensed-Regular.ttf" || exit r
curl -LOs "https://raw.githubusercontent.com/jpt/barlow/master/fonts/ttf/BarlowCondensed-Italic.ttf" || exit 1
curl -LOs "https://raw.githubusercontent.com/jpt/barlow/master/fonts/ttf/BarlowCondensed-Bold.ttf" || exit 1
curl -LOs "https://raw.githubusercontent.com/jpt/barlow/master/fonts/ttf/BarlowCondensed-BoldItalic.ttf" || exit 1

curl -LOs "https://raw.githubusercontent.com/RedHatBrand/Overpass/master/desktop-fonts/overpass/overpass-regular.otf" || exit 1
curl -LOs "https://raw.githubusercontent.com/RedHatBrand/Overpass/master/desktop-fonts/overpass/overpass-italic.otf" || exit 1
curl -LOs "https://raw.githubusercontent.com/RedHatBrand/Overpass/master/desktop-fonts/overpass/overpass-bold.otf" || exit 1
curl -LOs "https://raw.githubusercontent.com/RedHatBrand/Overpass/master/desktop-fonts/overpass/overpass-bold-italic.otf" || exit 1

curl -LOs "https://raw.githubusercontent.com/google/fonts/master/apache/opensans/OpenSans-Regular.ttf" || exit 1
curl -LOs "https://raw.githubusercontent.com/google/fonts/master/apache/opensans/OpenSans-Italic.ttf" || exit 1
curl -LOs "https://raw.githubusercontent.com/google/fonts/master/apache/opensans/OpenSans-BoldItalic.ttf" || exit 1
curl -LOs "https://raw.githubusercontent.com/google/fonts/master/apache/opensans/OpenSans-Bold.ttf" || exit 1

curl -LOs "https://raw.githubusercontent.com/mozilla/Fira/master/otf/FiraSans-Regular.otf" || exit 1
curl -LOs "https://raw.githubusercontent.com/mozilla/Fira/master/otf/FiraSans-Italic.otf" || exit 1
curl -LOs "https://raw.githubusercontent.com/mozilla/Fira/master/otf/FiraSans-Bold.otf" || exit 1
curl -LOs "https://raw.githubusercontent.com/mozilla/Fira/master/otf/FiraSans-BoldItalic.otf" || exit 1

curl -LOs "https://raw.githubusercontent.com/mozilla/Fira/master/otf/FiraMono-Regular.otf" || exit 1
#curl -LOs "https://raw.githubusercontent.com/mozilla/Fira/master/otf/FiraMono-Italic.otf" || exit 1
curl -LOs "https://raw.githubusercontent.com/mozilla/Fira/master/otf/FiraMono-Bold.otf" || exit 1
#curl -LOs "https://raw.githubusercontent.com/mozilla/Fira/master/otf/FiraMono-BoldItalic.otf" || exit 1

curl -LOs "https://raw.githubusercontent.com/RedHatBrand/Overpass/master/desktop-fonts/overpass-mono/overpass-mono-regular.otf" || exit 1
#curl -LOs "https://raw.githubusercontent.com/RedHatBrand/Overpass/master/desktop-fonts/overpass-mono/overpass-mono-italic.otf" || exit 1
curl -LOs "https://raw.githubusercontent.com/RedHatBrand/Overpass/master/desktop-fonts/overpass-mono/overpass-mono-bold.otf" || exit 1
#curl -LOs "https://raw.githubusercontent.com/RedHatBrand/Overpass/master/desktop-fonts/overpass-mono/overpass-mono-bold-italic.otf" || exit 1

curl -LOs "https://raw.githubusercontent.com/polarsys/b612/master/fonts/ttf/B612-Regular.ttf" || exit 1
curl -LOs "https://raw.githubusercontent.com/polarsys/b612/master/fonts/ttf/B612-Bold.ttf" || exit 1
curl -LOs "https://raw.githubusercontent.com/polarsys/b612/master/fonts/ttf/B612-Italic.ttf" || exit 1
curl -LOs "https://raw.githubusercontent.com/polarsys/b612/master/fonts/ttf/B612-BoldItalic.ttf" || exit 1
curl -LOs "https://raw.githubusercontent.com/polarsys/b612/master/fonts/ttf/B612Mono-Regular.ttf" || exit 1
curl -LOs "https://raw.githubusercontent.com/polarsys/b612/master/fonts/ttf/B612Mono-Bold.ttf" || exit 1
curl -LOs "https://raw.githubusercontent.com/polarsys/b612/master/fonts/ttf/B612Mono-Italic.ttf" || exit 1
curl -LOs "https://raw.githubusercontent.com/polarsys/b612/master/fonts/ttf/B612Mono-BoldItalic.ttf" || exit 1

curl -LOs "https://raw.githubusercontent.com/ctrlcctrlv/TT2020/master/dist/TT2020StyleE-Regular.ttf" || exit 1
curl -LOs "https://raw.githubusercontent.com/ctrlcctrlv/TT2020/master/dist/TT2020StyleE-Italic.ttf" || exit 1
curl -LOs "https://raw.githubusercontent.com/ctrlcctrlv/TT2020/master/dist/TT2020StyleB-Regular.ttf" || exit 1
curl -LOs "https://raw.githubusercontent.com/ctrlcctrlv/TT2020/master/dist/TT2020StyleB-Italic.ttf" || exit 1

curl -LOs "https://raw.githubusercontent.com/ThomasJockin/lexend/master/fonts/ttf/LexendDeca-Regular.ttf" || exit 1

curl -LOs "http://mirrors.ctan.org/fonts/cm-unicode/fonts/otf/cmuntt.otf" || exit 1
curl -LOs "http://mirrors.ctan.org/fonts/cm-unicode/fonts/otf/cmunit.otf" || exit 1
curl -LOs "http://mirrors.ctan.org/fonts/cm-unicode/fonts/otf/cmuntb.otf" || exit 1
curl -LOs "http://mirrors.ctan.org/fonts/cm-unicode/fonts/otf/cmuntx.otf" || exit 1

curl -LOs "http://mirrors.ctan.org/fonts/lm/fonts/opentype/public/lm/lmmono10-regular.otf" || exit 1
curl -LOs "http://mirrors.ctan.org/fonts/lm/fonts/opentype/public/lm/lmmono10-italic.otf" || exit 1

#curl -LOs "https://raw.githubusercontent.com/google/fonts/master/ofl/alegreya/Alegreya-Regular.ttf" || exit 1
#curl -LOs "https://raw.githubusercontent.com/google/fonts/master/ofl/alegreya/Alegreya-Italic.ttf" || exit 1
#curl -LOs "https://raw.githubusercontent.com/google/fonts/master/ofl/alegreya/Alegreya-Bold.ttf" || exit 1
#curl -LOs "https://raw.githubusercontent.com/google/fonts/master/ofl/alegreya/Alegreya-BoldItalic.ttf" || exit 1
#curl -LOs "https://raw.githubusercontent.com/google/fonts/master/ofl/alegreyasans/AlegreyaSans-Regular.ttf" || exit 1
#curl -LOs "https://raw.githubusercontent.com/google/fonts/master/ofl/alegreyasans/AlegreyaSans-Italic.ttf" || exit 1
#curl -LOs "https://raw.githubusercontent.com/google/fonts/master/ofl/alegreyasans/AlegreyaSans-Bold.ttf" || exit 1
#curl -LOs "https://raw.githubusercontent.com/google/fonts/master/ofl/alegreyasans/AlegreyaSans-BoldItalic.ttf" || exit 1

curl -LOs "https://raw.githubusercontent.com/huertatipografica/Alegreya/master/fonts/otf/Alegreya-Regular.otf" || exit 1
curl -LOs "https://raw.githubusercontent.com/huertatipografica/Alegreya/master/fonts/otf/Alegreya-Italic.otf" || exit 1
curl -LOs "https://raw.githubusercontent.com/huertatipografica/Alegreya/master/fonts/otf/Alegreya-Bold.otf" || exit 1
curl -LOs "https://raw.githubusercontent.com/huertatipografica/Alegreya/master/fonts/otf/Alegreya-BoldItalic.otf" || exit 1
curl -LOs "https://raw.githubusercontent.com/huertatipografica/Alegreya-Sans/master/fonts/otf/AlegreyaSans-Regular.otf" || exit 1
curl -LOs "https://raw.githubusercontent.com/huertatipografica/Alegreya-Sans/master/fonts/otf/AlegreyaSans-Italic.otf" || exit 1
curl -LOs "https://raw.githubusercontent.com/huertatipografica/Alegreya-Sans/master/fonts/otf/AlegreyaSans-Bold.otf" || exit 1
curl -LOs "https://raw.githubusercontent.com/huertatipografica/Alegreya-Sans/master/fonts/otf/AlegreyaSans-BoldItalic.otf" || exit 1

curl -LOs "https://raw.githubusercontent.com/google/fonts/master/ofl/rosarivo/Rosarivo-Regular.ttf" || exit 1
curl -LOs "https://raw.githubusercontent.com/google/fonts/master/ofl/rosarivo/Rosarivo-Italic.ttf" || exit 1

curl -LOs "https://raw.githubusercontent.com/libertinus-fonts/libertinus/master/LibertinusSerif-Regular.otf" || exit 1
curl -LOs "https://raw.githubusercontent.com/libertinus-fonts/libertinus/master/LibertinusSerif-Italic.otf" || exit 1
curl -LOs "https://raw.githubusercontent.com/libertinus-fonts/libertinus/master/LibertinusSerif-Bold.otf" || exit 1
curl -LOs "https://raw.githubusercontent.com/libertinus-fonts/libertinus/master/LibertinusSerif-BoldItalic.otf" || exit 1

curl -LOs "https://raw.githubusercontent.com/libertinus-fonts/libertinus/master/LibertinusSans-Regular.otf" || exit 1
curl -LOs "https://raw.githubusercontent.com/libertinus-fonts/libertinus/master/LibertinusSans-Italic.otf" || exit 1
curl -LOs "https://raw.githubusercontent.com/libertinus-fonts/libertinus/master/LibertinusSans-Bold.otf" || exit 1
#curl -LOs "https://raw.githubusercontent.com/libertinus-fonts/libertinus/master/LibertinusSans-BoldItalic.otf" || exit 1

curl -LOs "https://raw.githubusercontent.com/libertinus-fonts/libertinus/master/LibertinusMono-Regular.otf" || exit 1

curl -LOs "https://www.latofonts.com/download/Lato2OFL.zip" || exit 1

curl -LOs "https://garamond.org/urw/GaramondNo8-Regular.ttf" || exit 1
curl -LOs "https://garamond.org/urw/GaramondNo8-Italic.ttf" || exit 1
curl -LOs "https://garamond.org/urw/GaramondNo8-Bold.ttf" || exit 1
curl -LOs "https://garamond.org/urw/GaramondNo8-Bold-Italic.ttf" || exit 1

curl -LOs "https://raw.githubusercontent.com/CatharsisFonts/Cormorant/master/2.%20OpenType%20Files/CormorantGaramond-Medium.otf" || exit 1
curl -LOs "https://raw.githubusercontent.com/CatharsisFonts/Cormorant/master/2.%20OpenType%20Files/CormorantGaramond-MediumItalic.otf" || exit 1
curl -LOs "https://raw.githubusercontent.com/CatharsisFonts/Cormorant/master/2.%20OpenType%20Files/CormorantGaramond-Bold.otf" || exit 1
curl -LOs "https://raw.githubusercontent.com/CatharsisFonts/Cormorant/master/2.%20OpenType%20Files/CormorantGaramond-BoldItalic.otf" || exit 1

curl -LOs "https://raw.githubusercontent.com/google/fonts/master/ofl/ebgaramond/EBGaramond-Regular.ttf" || exit 1
curl -LOs "https://raw.githubusercontent.com/google/fonts/master/ofl/ebgaramond/EBGaramond-Italic.ttf" || exit 1
curl -LOs "https://raw.githubusercontent.com/google/fonts/master/ofl/ebgaramond/EBGaramond-Bold.ttf" || exit 1
curl -LOs "https://raw.githubusercontent.com/google/fonts/master/ofl/ebgaramond/EBGaramond-BoldItalic.ttf" || exit 1

curl -LOs "https://raw.githubusercontent.com/YuanshengZhao/Garamond-Math/master/Garamond-Math.otf" || exit 1

curl -LOs "https://raw.githubusercontent.com/googlefonts/ibarrareal/master/fonts/otf/IbarraRealNova-Regular.otf" || exit 1
curl -LOs "https://raw.githubusercontent.com/googlefonts/ibarrareal/master/fonts/otf/IbarraRealNova-Italic.otf" || exit 1
curl -LOs "https://raw.githubusercontent.com/googlefonts/ibarrareal/master/fonts/otf/IbarraRealNova-Bold.otf" || exit 1
curl -LOs "https://raw.githubusercontent.com/googlefonts/ibarrareal/master/fonts/otf/IbarraRealNova-BoldItalic.otf" || exit 1

curl -LOs "https://raw.githubusercontent.com/google/fonts/master/ofl/asap/Asap-Regular.ttf" || exit 1
curl -LOs "https://raw.githubusercontent.com/google/fonts/master/ofl/asap/Asap-Italic.ttf" || exit 1
curl -LOs "https://raw.githubusercontent.com/google/fonts/master/ofl/asap/Asap-Bold.ttf" || exit 1
curl -LOs "https://raw.githubusercontent.com/google/fonts/master/ofl/asap/Asap-BoldItalic.ttf" || exit 1

curl -LOs "https://raw.githubusercontent.com/impallari/Libre-Baskerville/master/LibreBaskerville-Regular.ttf" || exit 1
curl -LOs "https://raw.githubusercontent.com/impallari/Libre-Baskerville/master/LibreBaskerville-Italic.ttf" || exit 1
curl -LOs "https://raw.githubusercontent.com/impallari/Libre-Baskerville/master/LibreBaskerville-Bold.ttf" || exit 1

curl -LOs "https://raw.githubusercontent.com/theleagueof/sorts-mill-goudy/master/OFLGoudyStM.otf" || exit 1
curl -LOs "https://raw.githubusercontent.com/theleagueof/sorts-mill-goudy/master/OFLGoudyStM-Italic.otf" || exit 1

curl -LOs "https://raw.githubusercontent.com/skosch/Crimson/master/Desktop%20Fonts/OTF/Crimson-Roman.otf" || exit 1
curl -LOs "https://raw.githubusercontent.com/skosch/Crimson/master/Desktop%20Fonts/OTF/Crimson-Italic.otf" || exit 1
curl -LOs "https://raw.githubusercontent.com/skosch/Crimson/master/Desktop%20Fonts/OTF/Crimson-Bold.otf" || exit 1
curl -LOs "https://raw.githubusercontent.com/skosch/Crimson/master/Desktop%20Fonts/OTF/Crimson-BoldItalic.otf" || exit 1
####
curl -LOs "http://mirrors.ctan.org/fonts/cochineal/opentype/Cochineal-Roman.otf" || exit 1
curl -LOs "http://mirrors.ctan.org/fonts/cochineal/opentype/Cochineal-Italic.otf" || exit 1
curl -LOs "http://mirrors.ctan.org/fonts/cochineal/opentype/Cochineal-Bold.otf" || exit 1
curl -LOs "http://mirrors.ctan.org/fonts/cochineal/opentype/Cochineal-BoldItalic.otf" || exit 1

curl -LOs "https://raw.githubusercontent.com/shinntype/bellefair/gh-pages/fonts/otf/Bellefair-Regular.otf" || exit 1

# TODO: make this auto latest version
curl -LOs "http://vollkorn-typeface.com/download/${VOLLKORN}" || exit 1

curl -LOs "https://brill.com/fileasset/downloads_static/${BRILL}" || exit 1

curl -LOs "http://greekfontsociety-gfs.gr/_assets/fonts/${GFSDIDOT}" || exit 1
curl -LOs "http://greekfontsociety-gfs.gr/_assets/fonts/${GFSNEOHELLENIC}" || exit 1
curl -LOs "http://greekfontsociety-gfs.gr/_assets/fonts/${GFSBASKERVILLE}" || exit 1
curl -LOs "http://greekfontsociety-gfs.gr/_assets/fonts/${GFSELPIS}" || exit 1

curl -LOs "https://software.sil.org/downloads/r/gentium/${GENTIUM}" || exit 1
curl -LOs "https://software.sil.org/downloads/r/gentium/${GENTIUMBASIC}" || exit 1
curl -LOs "https://software.sil.org/downloads/r/andika/${ANDIKA}" || exit 1
curl -LOs "https://software.sil.org/downloads/r/andika/${ANDIKANEWBASIC}" || exit 1
curl -LOs "https://software.sil.org/downloads/r/charis/${CHARIS}" || exit 1
curl -LOs "https://software.sil.org/downloads/r/awami/${AWAMI}" || exit 1

curl -LOs "http://users.teilar.gr/~g1951d/Aegean.zip" || exit 1

curl -LOs "http://bureauroffa.com/s/ProzaRegular.zip" || exit 1

curl -LOs "https://raw.githubusercontent.com/g0v/moedict-webkit/master/fonts/MOEDICT.otf" || exit 1

#FREEHKVERSION=$(curl -Ls "https://freehkfonts.opensource.hk/download" | grep -Ei '<p\b.*自由香港楷書.*(4700字).*[[:digit:].]+' | grep -Eo '[[:digit:].]+\.[[:digit:].]+')
#curl -LOs "https://freehkfonts.opensource.hk/fonts/Free-HK-Kai_4700-v${FREEHKVERSION}.ttf" || exit 1

HANAZONOVERSION=$(curl -Ls "https://api.github.com/repos/cjkvi/HanaMinAFDKO/releases/latest" | grep 'Version' | grep -Eo '[[:digit:].]+')
# curl -LOs "https://github.com/cjkvi/HanaMinAFDKO/releases/download/${HANAZONOVERSION}/HanaMinA.otf" || exit 1
# curl -LOs "https://github.com/cjkvi/HanaMinAFDKO/releases/download/${HANAZONOVERSION}/HanaMinB.otf" || exit 1
# curl -LOs "https://github.com/cjkvi/HanaMinAFDKO/releases/download/${HANAZONOVERSION}/HanaMinC.otf" || exit 1
# curl -LOs "https://github.com/cjkvi/HanaMinAFDKO/releases/download/${HANAZONOVERSION}/HanaMinI.otf" || exit 1
# curl -LOs "https://github.com/cjkvi/HanaMinAFDKO/releases/download/${HANAZONOVERSION}/HanaMinExA1.otf" || exit 1
# curl -LOs "https://github.com/cjkvi/HanaMinAFDKO/releases/download/${HANAZONOVERSION}/HanaMinExA2.otf" || exit 1
# curl -LOs "https://github.com/cjkvi/HanaMinAFDKO/releases/download/${HANAZONOVERSION}/HanaMinExB.otf" || exit 1
# curl -LOs "https://github.com/cjkvi/HanaMinAFDKO/releases/download/${HANAZONOVERSION}/HanaMinExC.otf" || exit 1

# curl -LOs "https://oscdl.ipa.go.jp/IPAmjMincho/ipamjm00501.zip" || exit 1

  curl -LOs "http://babelstone.co.uk/Fonts/Download/BabelStoneHan.ttf" || exit 1
# curl -LOs "http://babelstone.co.uk/Fonts/Download/BabelStoneTangutWenhai.ttf" || exit 1
  curl -LOs "http://babelstone.co.uk/Fonts/Download/TangutYinchuan.ttf" || exit 1

curl -LOs "$(curl -Ls https://software.sil.org/scheherazade/download/ | grep -B 2 CURRENT | grep -Eo '"http.*software.*\.zip"' | sed -E 's|"(.*)"|\1|g')" || exit 1

curl -Ls -o "${INPUT}" 'http://input.fontbureau.com/build/?fontSelection=whole&a=0&g=0&i=serif&l=serif&zero=0&asterisk=height&braces=0&preset=default&line-height=1&accept=I+do' || exit 1

wait

unzip "${ROBOTO}" Roboto-Regular.otf Roboto-Italic.otf Roboto-Bold.otf Roboto-BoldItalic.otf &

unzip -j LastResort*.zip

unzip -j *iosevka-term-slab-*.zip \
	ttf-unhinted/iosevka-term-slab-regular.ttf \
	ttf-unhinted/iosevka-term-slab-italic.ttf \
	ttf-unhinted/iosevka-term-slab-bold.ttf \
	ttf-unhinted/iosevka-term-slab-bolditalic.ttf &

unzip -j *iosevka-term-ss08-*.zip \
	ttf-unhinted/iosevka-term-ss08-regular.ttf \
	ttf-unhinted/iosevka-term-ss08-italic.ttf \
	ttf-unhinted/iosevka-term-ss08-bold.ttf \
	ttf-unhinted/iosevka-term-ss08-bolditalic.ttf &

unzip -j coelacanth*.zip '*.otf' &

#unzip "${NOTO}" -x '*CJK*' '*Display*' '*Condensed*' '*-Thin*' '*-ExtraLight*' '*-Light*' '*-DemiLight*' '*-Medium*' '*-SemiBold*' '*-ExtraBold*' '*-Black*' '*LICENSE*' '*README*' &
unzip -j "${GENTIUM}" '*/GentiumPlus-*.ttf' &
unzip -j "${GENTIUMBASIC}" '*/Gen*Bas*.ttf' &
unzip -j "${ANDIKA}" '*/Andika-*.ttf' &
unzip -j "${ANDIKANEWBASIC}" '*/AndikaNewBasic-*.ttf' &
unzip -j "${CHARIS}" '*/CharisSIL-*.ttf' &
unzip -j "${AWAMI}" '*/AwamiNastaliq-*.ttf' &
unzip -j "${INPUT}" '*/InputMono-Regular.ttf' '*/InputMono-Italic.ttf' '*/InputMono-Bold.ttf' '*/InputMono-BoldItalic.ttf' &
unzip -j "${GFSDIDOT}" '*/GFS*Didot_Classic.otf' -x '__*' &
unzip -j "${GFSNEOHELLENIC}" '*/GFS*Neo[Hh]ellenic*.otf' -x '__*' &
unzip -j "${GFSBASKERVILLE}" '*/GFS*Baskerville.otf' -x '__*' &
unzip -j "${GFSELPIS}" '*/GFS*Elpis.otf' -x '__*' &

unzip -j Aegean.zip 'Aegean.ttf' &

unzip -j Scheherazade-*.zip '*/Scheherazade-*.ttf' &

unzip -j "${COELACANTH}" &

unzip -j "${VOLLKORN}" 'PS-OTF/*.otf' &

unzip -j "${BRILL}" '*/*.ttf' &&
mv Brill*Roman*.ttf      Brill-Roman.ttf &&
mv Brill*Italic*.ttf     Brill-Italic.ttf &&
mv Brill*Bold*.ttf       Brill-Bold.ttf &&
mv Brill*Bold*Italic*.ttf Brill-BoldItalic.ttf & # ONLY ONE &

unzip -j Lato*.zip '*/*.ttf' &

unzip -j Inter*.zip '*/*.otf' -x 'BETA' &

unzip -j ProzaRegular.zip '*/Proza-Regular.otf' &

#unzip -j Open_Data.zip '*/TW-*.ttf' &

unzip -j ipamjm*.zip 'ipamjm.ttf' &


wait

#for FONT_ALSO_HAS_UI in $(ls -1 *UI* | sed -E 's:Noto(.*)UI-.*\.[ot]t[cf]:\1:g' | sort -u); do
#	rm Noto*${FONT_ALSO_HAS_UI}-*.*
#done
#rm "${NOTO}"

chmod 644 *.[OoTt][Tt][CcFf]

echo -n "Press enter to continue with install: "
read THIS_VARIABLE_ONLY_FOR_PRESSING_ENTER
sudo sh -c "
	mkdir -p /etc/fonts/global
	rm -rf /etc/fonts/global/*
	rm -rf /etc/fonts/global/.*
	mv $DIR/*.otf $DIR/*.ttf $DIR/*.ttc /etc/fonts/global/
	chown root:root /etc/fonts/global/*
	chmod 644 /etc/fonts/global/*
	rm -rf $DIR
	fc-cache -frsv
"
