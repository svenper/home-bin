#!/bin/sh -v

unset POSIXLY_CORRECT

NICE="nice -n 19"

PKG=${1}
if [ "${2}" != "" ]
then
OPTIONS="-o ${2}" fi

### TODO: make this account for /var/cache/xbps-src instead if -H is used
DIR="/var/tmp/void-packages-${PKG}"

rm -rf ${DIR} || return 1
git clone --depth 1 https://github.com/void-linux/void-packages ${DIR}
mkdir -p ${DIR}/hostdir/repocache
ln -s /var/cache/xbps-src/repocache/* ${DIR}/hostdir/repocache/
mkdir -p ${DIR}/hostdir
#cp -a /var/cache/xbps-src/sources ${DIR}/hostdir/

cd ${DIR} || return 1
for ANY in $(ls $(dirname $(command -v ${0}))/patches/templates)
do cp -a "$(dirname $(command -v ${0}))/patches/templates/${ANY}" ./srcpkgs/
done

cat << EOF >> ${DIR}/common/shlibs
libkpathsea.so.6 texlive2019-exec-libkpathsea-2019_1
libptexenc.so.1 texlive2019-exec-libptexenc-2019_1
libsynctex.so.2 texlive2019-exec-libsynctex-2019_1
libtexluajit.so.2 texlive2019-exec-libtexlua-2019_1
libtexlua53.so.5 texlive2019-exec-libtexlua-2019_1
EOF
#libtexluajit.so.2 texlive2019-exec-libtexluajit-2019_1
#libtexlua53.so.5 texlive2019-exec-libtexlua53-2019_1

sed -E  -e '/CFLAGS/s/-O./-O3 -fdiagnostics-color/g' \
	-e '/MAKEJOBS/s/^#//g' \
	-e '/XBPS_ALLOW_RESTRICTED/s/^#//g' \
	-e 's| ?--[^ ]+[^(musl)][ "]||g' -e '/XBPS_INSTALL_ARGS/s|$|"|g' \
	${DIR}/etc/defaults.conf > ${DIR}/etc/conf

echo 'repository=/var/cache/xbps' > /tmp/repos-remote.conf
cat ${DIR}/etc/repos-remote.conf | grep musl | grep -v nonfree >> /tmp/repos-remote.conf
sed -i 's|https://repo\.voidlinux\.eu/current |/var/cache/xbps |g' ${DIR}/etc/repos-remote.conf
mv /tmp/repos-remote.conf ${DIR}/etc/repos-remote.conf

${NICE} ${DIR}/xbps-src ${OPTIONS} -H /var/cache/xbps-src binary-bootstrap || return 1
${NICE} ${DIR}/xbps-src ${OPTIONS} -H /var/cache/xbps-src -C extract ${PKG} || return 1

cd $(find ${DIR}/masterdir/builddir -name "${PKG}-*" -type d) || return 1

case ${PKG} in
	dmenu | dwm | slock | st | surf )
		sed -Ei '/cp.*config\.h/d' ${DIR}/srcpkgs/${PKG}/template || return 1
		rm -f config.h || return 1
		cp config.def.h config.h || return 1
		;;
esac

if [ -x "$(dirname $(command -v ${0}))/patches/${PKG}" ]; then
	. "$(dirname $(command -v ${0}))/patches/${PKG}"
fi

${NICE} ${DIR}/xbps-src ${OPTIONS} -H /var/cache/xbps-src -C -f pkg ${PKG}

PROVIDED="$(
	cd ${DIR}/srcpkgs
	for XBPSLINK in *; do
		if [ "$(readlink ${XBPSLINK})" = "${PKG}" ]; then
			echo ${XBPSLINK}
		fi
	done
)"

if [ "$(xbps-query --property automatic-install ${PKG})" != "yes" ]; then
	MANUAL_CMD="xbps-pkgdb --mode manual ${PKG}"
else
	MANUAL_CMD="xbps-pkgdb --mode auto ${PKG}"
fi


echo -n "Press enter to continue with install: "
read THIS_VARIABLE_ONLY_FOR_PRESSING_ENTER
sudo sh -c "
	xbps-install -A -f --repository=/var/cache/xbps-src/binpkgs ${PKG} $(
		for DEPPKG in $(xbps-query --ignore-conf-repos --repository=${DIR}/hostdir/binpkgs --deps ${PKG} | sed -e 's:>=0$:-0_0:g' -e 's:>=:-:g'); do
			xbps-uhelper getpkgname ${DEPPKG} | grep -Eo $(echo ${PROVIDED} | tr '\n ' '|')
		done
	)
	${MANUAL_CMD}
	xbps-pkgdb --mode hold ${PKG}
"
