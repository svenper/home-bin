#!/bin/sh -v

BUMP_PACKAGE="${1}"
BUMP_VERSION="${2}"

if [ "${BUMP_PACKAGE}" = "" ] || [ "${BUMP_VERSION}" = "" ]; then
	exit 1
fi

cd /var/tmp/void-packages &&
git checkout master &&
git pull --rebase --autostash upstream master &&
git branch -D "${BUMP_PACKAGE}"

git checkout -b "${BUMP_PACKAGE}" &&
sed -E -i -e '/^version=/s/=.*$/='"${BUMP_VERSION}"'/g' srcpkgs/"${BUMP_PACKAGE}"/template &&
sed -E -i -e '/^revision=/s/=.*$/=1/g' srcpkgs/"${BUMP_PACKAGE}"/template &&
xgensum -f -i -H /var/cache/xbps-src srcpkgs/"${BUMP_PACKAGE}"/template &&
xbump "${BUMP_PACKAGE}"

cat << EOF


Now do this:

git push --force --set-upstream origin "${BUMP_PACKAGE}" &&
git checkout master &&
git branch -D "${BUMP_PACKAGE}"

EOF
